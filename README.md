# Plugin Test Task

This is my first experience in development eclipse plugins. With this plugin you can add new type of launch configuration. This configuration gives ability to make composite launch.

  - Create pack of configurations
  - Set delay between launches
  - Run

All stored configurations will be launched.

### Screenshot
Screenshot

### Instruction

You can see two areas.  
Left area - all configurations. Right area - configurations for launch.   
Plugin support double click actions. Double click on left area - add new launch configuration. Double click on right area - remove from configurations for launch.

Button "Clear" remove all configurations from right area. 

Delay means - time between launches of saved configurations in ms.

### Error messages

In plugin are some error messages.
You can view these if you try:

  - Add to right area current configuration (the same name in field "Name"). This action can cause endless call.
  - Add to right field configuration, which stored current configuration. This action can cause endless call.
  - Add to right field configuration, which currently added.

### Information sources

Here are sources with information, which was used to create this plugin.


* [Article-Launch-Framework] - basic article
* [Article-Your First Plug-in] 
* [Eclipse Help] - descriptions of classes, interfaces etc.
* [My first Eclipse Plugin] - article on habrahabr (russian). Helps a lot.
* [Progress Reporting]  
* [SWT Snippets] - Helps with UI
* [Article-Understanding-Layouts] - Article about layouts in plugins
* [Tree viewer] - Content provider, Label provider
* [Write your own launcher] - blog 
* [Extention points]



[Article-Launch-Framework]:http://www.eclipse.org/articles/Article-Launch-Framework/launch.html
[Article-Your First Plug-in]:http://www.eclipse.org/articles/Article-Your%20First%20Plug-in/YourFirstPlugin.html
[Eclipse Help]:http://help.eclipse.org/
[My first Eclipse Plugin]:http://habrahabr.ru/post/243297/
[SWT Snippets]:http://www.eclipse.org/swt/snippets/
[Article-Understanding-Layouts]:http://www.eclipse.org/articles/article.php?file=Article-Understanding-Layouts/index.html
[Progress Reporting]:https://wiki.eclipse.org/Progress_Reporting
[Tree viewer]:http://www.eclipse.org/articles/Article-TreeViewer/TreeViewerArticle.htm
[Write your own launcher]:http://dianaseclipsenotes.blogspot.com/2010/07/write-your-own-launcher-in-eclipse.html
[Extention points]:https://dzone.com/articles/when-extend-which-extension-0
