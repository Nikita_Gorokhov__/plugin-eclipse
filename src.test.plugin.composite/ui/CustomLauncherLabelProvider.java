package ui;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class CustomLauncherLabelProvider extends LabelProvider {

	@Override
	public Image getImage(Object element) {
		try {
			if (element instanceof ILaunchConfigurationType) {
				return DebugUITools.getImage(((ILaunchConfigurationType) element).getIdentifier());
			} else {
				return DebugUITools.getImage(((ILaunchConfiguration) element).getType().getIdentifier());
			}	
		} catch (CoreException e) {
			return null;
		}
	}

	@Override
	public String getText(Object element) {
		if (element instanceof ILaunchConfigurationType) {
			return ((ILaunchConfigurationType) element).getName();
		} else if (element instanceof ILaunchConfiguration) {
			return ((ILaunchConfiguration) element).getName();
		}
		return null;
	}
}
