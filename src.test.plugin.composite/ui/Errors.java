package ui;

import org.eclipse.jface.dialogs.MessageDialog;

/**
 * Class provides different error messages
 *
 */
public class Errors {
	public static final String ERROR_MSG_1 = "Can't add this configurations. This is current configuration.";
	
	public static final String ERROR_MSG_2 = "Can't add this configurations. This configration has a link to current configuration. Circular dependency.";
	
	public static final String ERROR_MSG_3 = "Can't add this configurations. This configration currently available in list.";
	
	public static void showErrorMessage(final String string) {
		final MessageDialog msg = new MessageDialog(null, "Error", null, string, MessageDialog.ERROR, new String[]{"OK"}, 0);
		msg.open();
	}
}
