package ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;


public class CustomLauncherTreeViewProvider implements ITreeContentProvider  {
	
	private List<ILaunchConfiguration> launchConfigurations;
	private String launchMode;
	
	public CustomLauncherTreeViewProvider(String launchMode) {
		this.launchMode = launchMode;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object[] getElements(Object inputElement) {
		launchConfigurations = (List<ILaunchConfiguration>) inputElement;
		try {
			// create set of configuration types
			final Set<ILaunchConfigurationType> configurationSet = new HashSet<>();
			// check types for all available configurations
			for (ILaunchConfiguration configuration : launchConfigurations) {
				if (configuration.supportsMode(launchMode))
					configurationSet.add(configuration.getType());
			}
			return new ArrayList<>(configurationSet).toArray();
		} catch (CoreException exception) {
			return Collections.EMPTY_LIST.toArray();
		}
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		// if element is a type
		if (parentElement instanceof ILaunchConfigurationType) {
			final List<ILaunchConfiguration> childConfigs = new ArrayList<>();
			// get all child elements for type
			for (ILaunchConfiguration config: launchConfigurations) {
				if (getLaunchConfType(config).equals(parentElement)) {
					childConfigs.add(config);
				}
			}
			return childConfigs.toArray();
		} else {
			return Collections.EMPTY_LIST.toArray();
		}	
	}

	@Override
	public Object getParent(Object element) {
		// return launch type for child element
		return element instanceof ILaunchConfiguration ? getLaunchConfType((ILaunchConfiguration) element) : null;
	}

	@Override
	public boolean hasChildren(Object element) {
		// if element is launch type - has childs
		return element instanceof ILaunchConfigurationType;
	}
	
	/**
	 * Get configuration type
	 * @param launchConfiguration
	 * @return Type of {@code launchConfiguration}
	 */
	private static ILaunchConfigurationType getLaunchConfType(ILaunchConfiguration launchConfiguration) {
		try {
			return launchConfiguration.getType();
		} catch (CoreException exception) {
			return null;
		}
	}

}
