package ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.FilteredTree;
import org.eclipse.ui.dialogs.PatternFilter;

import launcher.CompositeLaunchUtils;

/**
 * Custom tab of launch configuration
 *
 */
public class CompositeLaunchConfTab extends AbstractLaunchConfigurationTab {
	
	private static final String DELAY_LABEL = "Delay beetwen launch configurations (ms):";

	// tab name 
	private static final String TAB_NAME = "Test Composite Plugin";
	
	// current open configuration
	private String configurationName;
	
	// saved configurations 
	private List<ILaunchConfiguration> storedConfigurations = new ArrayList<>();
	
	// delay between launches
	private String delayTime = "0";
	
	// tree shows stored configurations
	private TreeViewer addedTree;
	
	// delayTime textbox
	private Text text;

	// launch mode (run, debug)
	private String launchMode;
	
	public CompositeLaunchConfTab(final String launchMode) {
		this.launchMode = launchMode;
	}
	

	@Override
	public void createControl(Composite parent) {
		final Composite parentContainer = new Composite(parent, SWT.NONE);
		parentContainer.setFont(parent.getFont());
		parentContainer.setLayout(new GridLayout(2, true));
		createFilteredTreeView(parentContainer);
		createAddedTreeView(parentContainer);
		createDelayContainer(parentContainer);
		createClearButton(parentContainer);

		setControl(parentContainer);
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
		storedConfigurations.clear();
	}

	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		configurationName = configuration.getName();
		storedConfigurations.clear();
		storedConfigurations.addAll(CompositeLaunchUtils.getStoredLaunchConfigurations(configuration));
		delayTime = CompositeLaunchUtils.getDelayTimeBeweenLaunches(configuration).toString();
		update();
		text.setText(delayTime);
	}

	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		List<String> configNames = new ArrayList<>();
		// create list of names of stored configurations
		for (ILaunchConfiguration config: storedConfigurations) {
			configNames.add(config.getName());
		}
		// save values of configuration
		configuration.setAttribute(CompositeLaunchUtils.STORED_CONFIGURATIONS, configNames);
		configuration.setAttribute(CompositeLaunchUtils.DELAY_TIME, delayTime);
	}

	@Override
	public String getName() {
		return TAB_NAME;
	}
	
	/**
	 * Left area with all available configurations
	 * @param parent - main container
	 */
	private void createFilteredTreeView (final Composite parent) {
		final GridData gridData = new GridData(SWT.FILL,SWT.FILL,true,true);	
		FilteredTree filteredTree = new FilteredTree(parent, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL, new PatternFilter(),true);
		filteredTree.getFilterControl().setLayoutData(gridData);
		TreeViewer treeViewer = filteredTree.getViewer();
		treeViewer.setContentProvider(new CustomLauncherTreeViewProvider(launchMode));
		treeViewer.setLabelProvider(new CustomLauncherLabelProvider());
		treeViewer.setInput(CompositeLaunchUtils.getAllValidConfigurations(launchMode));
		treeViewer.addDoubleClickListener(new IDoubleClickListener() {
			
			// double click - move configuration to stored collection
			@Override
			public void doubleClick(DoubleClickEvent event) {
				// get selected item
				final TreeSelection configuration = (TreeSelection) event.getSelection();
				final Object element = configuration.getFirstElement();
				if (element instanceof ILaunchConfiguration) {
					// can we add this configuration
					if (isConfigCanBeAdded((ILaunchConfiguration) element)) {
						storedConfigurations.add((ILaunchConfiguration) element);
						update();
					}
				}
			}
		});
	}
	
	/**
	 * right area with selected configurations
	 * @param parent - main container
	 */
	private void createAddedTreeView (final Composite parent) {
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		addedTree = new TreeViewer(parent, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL );
		addedTree.getControl().setLayoutData(gridData);
		addedTree.setContentProvider(new CustomLauncherTreeViewProvider(launchMode));
		addedTree.setLabelProvider(new CustomLauncherLabelProvider());
		addedTree.setInput(storedConfigurations);
		addedTree.addDoubleClickListener(new IDoubleClickListener() {
			
			// double click - remove from stored collection
			@Override
			public void doubleClick(DoubleClickEvent event) {
				final TreeSelection configuration = (TreeSelection) event.getSelection();
				final Object element = configuration.getFirstElement();
				if (element instanceof ILaunchConfiguration) {
					storedConfigurations.remove(storedConfigurations.indexOf(element));
					update();
				}
			}
		});
	}

	/**
	 * Label and text box for delay time value
	 * @param parent - main container
	 */
	private void createDelayContainer (final Composite parent) {
		final Composite composite = new Composite(parent, SWT.FILL);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		composite.setLayout(new GridLayout(2,false));
		composite.setLayoutData(gridData);
		// create label
		Label label = new Label(composite, SWT.FILL);
		label.setText(DELAY_LABEL);
		label.setFont(composite.getFont());
		
		//create text box
		text = new Text(composite, SWT.BORDER | SWT.MAX);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		text.setLayoutData(gridData);
		text.setText(delayTime);

		// write changes to variable
		text.addListener(SWT.Modify, new Listener () {
			@Override
			public void handleEvent (Event e) {
				if (text.getText().isEmpty()) {
					text.setText("0");
				}			
				delayTime = text.getText();
			}
		});
		// verify, write only numbers
		text.addListener(SWT.Verify, new Listener () {
			@Override
			public void handleEvent (Event e) {
				String string = e.text;
				char [] chars = new char [string.length ()];
				string.getChars (0, chars.length, chars, 0);
				for (int i=0; i<chars.length; i++) {
					if (!('0' <= chars [i] && chars [i] <= '9')) {
						e.doit = false;
						return;
					}
				}
			}
		});

	}

	/**
	 * Clear all button - remove all stored configs
	 * @param parent - main container
	 */
	private void createClearButton(final Composite parent) {
		final Composite composite = new Composite(parent, SWT.NONE);
		final GridData data = new GridData(SWT.RIGHT,SWT.NONE,true,false);

		composite.setLayout(new GridLayout(1, false));
		composite.setFont(parent.getFont());
		composite.setLayoutData(data);
		final Button clearButton = createPushButton(composite, "Clear",null);
		clearButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// remove all from stored collection
				storedConfigurations.clear();
				update();
			}
		});
	}
	
	/**
	 * Refresh dialog and right area after changes
	 */
	private void update () {
		updateLaunchConfigurationDialog();
		addedTree.refresh();
		addedTree.expandAll();
	}

	/**
	 * @param launchConfiguration - adding configuration
	 * @return can {@code launchConfiguration} be stored 
	 */
	private boolean isConfigCanBeAdded (final ILaunchConfiguration launchConfiguration) {
		// if try to add the same config
		if (launchConfiguration.getName().equals(configurationName)) {
			Errors.showErrorMessage(Errors.ERROR_MSG_1);
			return false;
		}
		// if try to add config with link to current config
		final List<ILaunchConfiguration> innerConfigurations = CompositeLaunchUtils.getStoredLaunchConfigurations(launchConfiguration);
		for (ILaunchConfiguration config: innerConfigurations) {
			if (config.getName().equals(configurationName)) {
				Errors.showErrorMessage(Errors.ERROR_MSG_2);
				return false;
			}
		}
		// if try to add available config
		for (ILaunchConfiguration config: storedConfigurations) {
			if (config.getName().equals(launchConfiguration.getName())) {
				Errors.showErrorMessage(Errors.ERROR_MSG_3);
				return false;
			}
		}
		return true;
	}

}
