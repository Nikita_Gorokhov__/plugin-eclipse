package launcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;

/**
 * Utils class
 *
 */
public class CompositeLaunchUtils {

	public final static String STORED_CONFIGURATIONS = "Stored configurations";
	public static final String DELAY_TIME = "Delay time";

	/**
	 * Get stored configurations
	 * @param configuration
	 * @return all stored configurations from {@code configuration}
	 */
	public static List<ILaunchConfiguration> getStoredLaunchConfigurations(final ILaunchConfiguration configuration) {

		final List<ILaunchConfiguration> storedLaunchConfigurations = new ArrayList<>();
		try {
			// get all stored configuration names by attribute
			final List<String> storedConfigNames = configuration.getAttribute(STORED_CONFIGURATIONS,
					Collections.emptyList());

			// get all configurations
			final List<ILaunchConfiguration> allCongifurations = Arrays
					.asList(DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurations());

			// get configurations by name
			for (String name : storedConfigNames) {
				for (ILaunchConfiguration config : allCongifurations) {
					if (config.getName().equals(name)) {
						storedLaunchConfigurations.add(config);
					}
				}
			}
		} catch (CoreException e) {
			return Collections.emptyList();
		}
		return storedLaunchConfigurations;
	}

	/**
	 * @param mode - supported mode
	 * @return all supports {@code mode} configurations 
	 */
	public static List<ILaunchConfiguration> getAllValidConfigurations(final String mode) {
		try {
			// all configurations
			final List<ILaunchConfiguration> allConfigurations = Arrays
					.asList(DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurations());
			final List<ILaunchConfiguration> validConfigurations = new ArrayList<>();
			// configuration validation
			for (ILaunchConfiguration config : allConfigurations) {
				if (isValidConfiguration(config, mode)) {
					validConfigurations.add(config);
				}
			}
			return validConfigurations;
		} catch (CoreException exception) {
			return Collections.emptyList();
		}
	}

	/**
	 * @param configuration
	 * @return delay time between launches of stored configurations
	 */
	public static Long getDelayTimeBeweenLaunches(final ILaunchConfiguration configuration) {
		try {
			String delayTime = configuration.getAttribute(DELAY_TIME, "0");
			return Long.parseLong(delayTime);
		} catch (CoreException e) {
			return 0l;
		}

	}

	/**
	 * @param configuration - checked configuration
	 * @param mode - supports mode
	 * @return is {@code configuration} supports {@code mode}
	 */
	private static boolean isValidConfiguration(final ILaunchConfiguration configuration, final String mode) {
		try {
			return (configuration.supportsMode(mode) || mode == null);
		} catch (CoreException e) {
			return false;
		}
	}

}
