package launcher;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.debug.core.model.IProcess;

/**
 * launch configuration delegate custom implementation
 */
public class CompositeLaunchConfigurationImpl implements ILaunchConfigurationDelegate {
	
	@Override
	public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor)
			throws CoreException {
		try {
			// get all saved configuration
			final List<ILaunchConfiguration> storedConfigurations = CompositeLaunchUtils
					.getStoredLaunchConfigurations(configuration);

			// used for progress reporting
			final SubMonitor progress = SubMonitor.convert(monitor, configuration.getName(),
					storedConfigurations.size());

			// delay between launches
			final Long delayTime = CompositeLaunchUtils.getDelayTimeBeweenLaunches(configuration);
			for (final ILaunchConfiguration storedConfiguration : storedConfigurations) {
				if (!monitor.isCanceled()) {

					final ILaunch result = storedConfiguration.launch(mode, progress.newChild(1));

					// get and attach all debug target from storedConfiguration
					// launch
					for (IDebugTarget target : result.getDebugTargets()) {
						launch.addDebugTarget(target);
					}

					// get and attach all processes from storedConfiguration
					// launch
					for (IProcess process : result.getProcesses()) {
						launch.addProcess(process);
					}
					if (delayTime != null && delayTime > 0) {
						Thread.sleep(delayTime);
					}
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			if (monitor != null) {
				monitor.done();
			}
		}

	}

}
